I created to support my undergraduate economic forecasting and graduate time series econometrics classes.
The functionality should also be useful for research.
Currently there is no good documentation. I've started documenting things [on this site](https://bachmeil.github.io/tstoolsdoc/) but that's limited at this time (May 2021).
You may need to look at the source code.

# Installation


```
library(devtools)
install_bitbucket("bachmeil/tstools")
```

# Warning

In general, there shouldn't be code breakage, but I'm not making any promises. I'll be happy to break compatibility if there's a good reason.