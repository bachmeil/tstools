# delta.sum, se.sum, delta.cumsum, se.cumsum

```
delta.sum <- function(theta, v)
delta.cumsum <- function(theta, v)
se.sum <- function(fit, m)
se.cumsum <- function(fit, m)
```

`delta.sum` returns the standard error of the sum of the elements of `theta`.

`delta.cumsum` return the standard errors of the cumulative sums of the elements
of `theta`.

`se.sum` and `se.cumsum` are specialized convenience versions of `delta.sum` and
`delta.cumsum` taking regression output as an argument.

## Arguments

- `theta`: Vector of coefficients.
- `v`: The covariance matrix associated with the elements of `theta`.
    `v` has to be a square matrix with dimensions matching `theta`.
- `fit`: Output of a call to `lm`.
- `m`: A vector identifying the index numbers of the coefficients to use.

## Return Value

`delta.sum` and `se.sum` return a single value that is the standard deviation
of the sum of coefficients. `delta.cumsum` and `se.cumsum` return a vector
holding the standard deviations of the cumulative sums.

*Created: 2019/07/16*  
*Last Update: 2019/07/16*
