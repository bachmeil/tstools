# `tsreg`

`function(y.raw, x.raw, start=NULL, end=NULL, intercept=TRUE)`

Estimates a regression model similar to `lm`, but is designed to handle
time series data. Will align regressors covering different time periods.
Retains the `ts` properties of the residuals and fitted values.

## Examples

```
tsreg(y, lags(y, 1:3))
tsreg(y, ts.combine(lags(y, 1:3), lags(x, 1:3)), start=c(1998,1), 
      end=c(2012,12))
```

## Arguments

- `y.raw`: The `ts` object that will be on the left side of the regression.
- `x.raw`: The `ts` or `mts` object that will be the regressors. This can hold
    any set of `ts` objects, including lags of the variables, time trends,
    and dummy variables.
- `start`: [Optional, defaults to `NULL`] If you pass in a date, like `c(2010,1)`, it will use
    that as the earliest possible date for the regression sample. **Warning:**
    This option will have no effect, and you will not be given a warning,
    if the computed start date for the regression is after the value you set.
    For instance, in the regression `tsreg(y, x, start=c(2010,1))`, setting
    the value of start has no effect if `y` or `x` start in 2011.
- `end`: [Optional, defaults to `NULL`] The same as `start`, but it sets the latest possible
    end date for the sample.
- `intercept`: [Optional, defaults to `TRUE`] Set to `FALSE` if you do not want an intercept in
    the estimated model. The default is to have an intercept, so you should
    normally not need to set this parameter.

##  Return Value

`tsreg` returns the same output as a call to `lm`, and any function designed
to work with `lm` output can be applied to the output from a call to `tsreg`.
The following additional values are also returned as part of the list:

- `$resids`: The residual series, but as a `ts` object.
- `$fitted`: The fitted values, but as a `ts` object.
- `$start`: The starting date for the sample used to estimate the regression.
- `$end`: The ending date for the sample used to estimate the regression.
- `$int`: Boolean (`TRUE` or `FALSE`) indicating whether or not an intercept was included in the regression.

*Created: 2019/07/16*  
*Last Update: 2019/07/16*
