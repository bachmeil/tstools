import std.process, std.stdio;

void main(string[] args) {
	auto cmd = `cd /home/office/repos; R CMD build tstools`;
	writeln(cmd);
	auto v = executeShell(cmd);
	if (v.output != "") {
		writeln(v.output);
	}

	cmd = `scp /home/office/repos/tstools_0.` ~ args[1] ~ `.tar.gz root@104.236.221.250:/root/downloads`;
	writeln(cmd);
	v = executeShell(cmd);
	if (v.output != "") {
		writeln(v.output);
	}

	cmd = `ssh root@104.236.221.250 'R CMD INSTALL -l /usr/lib/R/library/ /root/downloads/tstools_0.` ~ args[1] ~ `.tar.gz'`;
	writeln(cmd);
	v = executeShell(cmd);
	if (v.output != "") {
		writeln(v.output);
	}
}
